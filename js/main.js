const devon = document.querySelectorAll(".contactlist__profile");
const listUser = document.getElementById("contactlist");

const detail = document.getElementById("chatdetail");
const back = document.querySelector(".fa-chevron-left");

for (i = 0; i < 6; i++) {
  devon[i].onclick = function () {
    listUser.classList.add("hide");
    detail.classList.remove("hide");
  };
}

back.onclick = function () {
  detail.classList.add("hide");
  listUser.classList.remove("hide");
};
